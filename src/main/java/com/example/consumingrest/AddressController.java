package com.example.consumingrest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class AddressController {

    @GetMapping("/{cep}")
    public String treatJson(RestTemplate restTemplate, @PathVariable String cep) {
        Address address = restTemplate.getForObject(
                String.format("https://viacep.com.br/ws/%s/json/", cep),
                Address.class);

        if (address.getCep() != null) {
            if (address.getLogradouro() == null) {
                return address.toString(cep);
                // return String.format("Endereço: %s/%s (%s)", address.getLocalidade(),
                // address.getUf(), cep);
            } else {
                return address.toString();
            }
        } else {
            return "CEP não encontrado";
        }
    }
}